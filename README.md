 # **Solucion**
 ## ***Pregunta 1***
 Crear un repositorio remoto con 3 ramas, el cual tendrá un archivo txt y tendrá las siguientes palabras por cada rama: 

        • Bienvenido → rama 1 
 <div> <img src="./img/p1-0.jpg"/> </div>
 -----------------------------------------------

        • Bienvenido al → rama 2 
 <div> <img src="./img/p1-1.jpg"/> </div>
 -----------------------------------------------

        • Bienvenido al aprendizaje → rama 
 <div> <img src="./img/p1-2.jpg"/> </div>

 ## ***Pregunta 2***
 Crear un repositorio remoto con 3 ramas, el cual tendrá un archivo txt y tendrá las siguientes palabras por cada rama: 

        • Manejando → rama 1 
 <div> <img src="./img/p2-0.jpg"/> </div>
 -----------------------------------------------

        • Comandos en → rama 2 
 <div> <img src="./img/p2-1.jpg"/> </div>
 -----------------------------------------------

        • Git → rama 
 <div> <img src="./img/p2-2.jpg"/> </div>
 
 ## ***Pregunta 3***
 Mostrar el log y log --oneline del punto 1 y 2

        • Punto uno
 <div> <img src="./img/p3-p1 log.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p3-p1 log --oneline.jpg"/> </div>
 -----------------------------------------------
 
        • Punto dos
 <div> <img src="./img/p3-p2 log.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p3-p2 log --oneline.jpg"/> </div>
 
 ## ***Pregunta 4***
 Mostrar la aplicación del comando git commit -am con archivos nuevos y modificados, con la estructura de carpetas y archivos que usted quiera

 <div> <img src="./img/p4-status am anadiendo.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p4-status am modificado.jpg"/> </div>
 -----------------------------------------------

 ## ***Pregunta 5***
 Mostrar la modificación de la estructura de un proyecto, con la estructura de carpetas y archivos que usted quiera

 <div> <img src="./img/p5.jpg"/> </div>

 ## ***Pregunta 6***
 Mostrar la salida de archivos del área de preparación al espacio de trabajo, con la estructura de carpetas y archivos que usted quiera

 <div> <img src="./img/p6.jpg"/> </div>

 ## ***Pregunta 7***
 Mostrar el uso de reset, reset mixed, reset soft y reset hard, con la estructura de carpetas y archivos que usted quiera

 <div> <img src="./img/p7-reset,reset --mixed.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p7-reset --soft.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p7-reset --hard.jpg"/> </div>
 
 ## ***Pregunta 8***
 Mostrar el cambio y borrado de un archivo cualquiera

 <div> <img src="./img/p8.jpg"/> </div>
 
 ## ***Pregunta 9***
 Mostrar los cambios realizados a un archivo diff y diff --stat.

 <div> <img src="./img/p9.jpg"/> </div>
 
 ## ***Pregunta 10***
  Mostrar 3 ramas en local, uno para merge fast-forward, uno para merge automática y uno para merge manual, con la estructura de carpetas y archivos que usted quiera, al final borrar las ramas auxiliares de master. 
  
        • Automatico
 <div> <img src="./img/p10-automatico.jpg"/> </div>
 -----------------------------------------------
 
        • Fast Forward
 <div> <img src="./img/p10-fast-forward.jpg"/> </div>
 -----------------------------------------------
 
        • Manual
 <div> <img src="./img/p10-manual.jpg"/> </div>
 
 ## ***Pregunta 11***
  Mostrar la adicion y eliminación de un tag a una rama auxiliar

 <div> <img src="./img/p11.jpg"/> </div>
 
 ## ***Pregunta 12***
  Mostrar un git clone, push y pull, con la estructura de carpetas y archivos que usted quiera

        • Clone
 <div> <img src="./img/p12-clone.jpg"/> </div>
 -----------------------------------------------
 
        • Pull
 <div> <img src="./img/p12-pull.jpg"/> </div>
 -----------------------------------------------
 
        • Push
 <div> <img src="./img/p12-push.jpg"/> </div>
 
 ## ***Pregunta 13***
  Mostrar la creación y eliminación de ramas remotas, con la estructura de carpetas y archivos que usted quiera. 
  
        • Creando rama-remota
 <div> <img src="./img/p13-creando.jpg"/> </div>
 -----------------------------------------------
 
        • Eliminando rama-remota
 <div> <img src="./img/p13-eliminando0.jpg"/> </div>
 -----------------------------------------------
 
 <div> <img src="./img/p13-eliminando1.jpg"/> </div>
 -----------------------------------------------
 
 <div> <img src="./img/p13-eliminando2.jpg"/> </div>
 
 ## ***Pregunta 14***
 Realizar un fork con el repositorio de un compañero y viceversa, también realizar un fork de cualquier proyecto existente que se tenga en su repositorio
 
        • Fork de mi amiga
 <div> <img src="./img/p14-forkfriend.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p14-forkfriend0.jpg"/> </div>
 -----------------------------------------------
 
        • Fork de mi repositorio
 <div> <img src="./img/p14-forkmy.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p14-forkmy0.jpg"/> </div>
 
 ## ***Pregunta 15***
  Proteger la rama master y realizar merge request con un equipo de 2 a 5 personas, modificando el texto de cualquier archivo txt en su repositorio local, cada persona del equipo tomara el rol de líder de equipo, para realizar la protección de la rama master y aceptación de merge request. 
  
        • Developer
 <div> <img src="./img/p15-0.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p15-1.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p15-2.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p15-3.jpg"/> </div>
 -----------------------------------------------

        • Revision
 <div> <img src="./img/p15-revision0.jpg"/> </div>
 -----------------------------------------------
 <div> <img src="./img/p15-revision1.jpg"/> </div>